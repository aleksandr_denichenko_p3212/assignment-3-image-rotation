#pragma once

#include "image.h"
#include  <stdint.h>
#include <stdio.h>



#ifndef BMP_READER_H
#define DMP_READER_H

#pragma pack(push, 1) //делаем выравнивание для облегчения считывания
typedef struct BMPHeader{
	uint16_t bfType; //id
        uint32_t  bfileSize; //file size
        uint32_t bfReserved; //unused
        uint32_t bOffBits; //pixel offset
} BMPHeader;

typedef struct DIBHeader{
	uint32_t biSize; //header size
        uint32_t biWidth; //width
        uint32_t  biHeight; //height
        uint16_t  biPlanes; //color planes
        uint16_t biBitCount; //bits per pixel
        uint32_t biCompression; //compress type
        uint32_t biSizeImage; //data size
        uint32_t biXPelsPerMeter; //pwidth
        uint32_t biYPelsPerMeter; //pheight
        uint32_t biClrUsed; //colors count
        uint32_t  biClrImportant; //imp colors count
} DIBHeader;

typedef struct BMPFile{
	BMPHeader bhdr;
	DIBHeader dhdr;
	unsigned char* data;
} BMPFile;
#pragma pack(pop)

enum read_status {
    BMP_READ_OK = 0,
    BMP_READ_ERROR
};

enum read_status from_bmp(char *in, struct image *img);

enum write_status {
    BMP_WRITE_OK = 0,
    BMP_WRITE_ERROR
};

enum write_status to_bmp(char *out, struct image const *img);

#endif
