#pragma once

#include <stdint.h>

#pragma pack(push, 1)
struct pixel
{
    uint8_t b, g, r;
};
#pragma pack(pop)

struct image
{
    uint64_t width, height;
    struct pixel *data;
};

void initImage(struct image *img, uint64_t width, uint64_t height);
void freeImage(struct image *img);
struct pixel imageGetPixel(struct image const *img, uint64_t a, uint64_t b);
void imageSetPixel(struct image *img, uint64_t a, uint64_t b, struct pixel p);
