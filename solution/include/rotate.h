#pragma once

#include "image.h"

struct image rotateImage(struct image const* source, double angle);
