#include "bmp.h"
#include <stdio.h>
#include <stdlib.h>

// Для BMP файловые форматы
#define BYTES_PER_PIXEL 3
#define BMP_TYPE 0x4d42
#define BI_RGB 0
#define DIB_HEADER_SIZE_B 40
#define BITS_PER_PIXEL 24
#define BI_RGB 0

#ifndef BMP_READER_C
#define BMP_READER_C

void freeBMPFile(BMPFile *bmp_file)
{
	if (bmp_file)
	{
		if (bmp_file->data)
		{
			free(bmp_file->data);
			bmp_file->data = NULL;
		}
		free(bmp_file);
		bmp_file = NULL;
	}
}

// СМОТРЕТЬ ТОЛЬКО ПРАКТИКУ, ФУНКЦИЯ НАПИСАНА ДЛЯ КАСТОМНОГО РЕЖИМА 24, 32 битности
// void readBMPRowByRow(FILE *fp, BMPFile *bmpf)
// {
// 	size_t bytes_per_pixel = bmpf->dhdr.biBitCount / 8;
// 	size_t row_size = bytes_per_pixel * bmpf->dhdr.biWidth;
// 	size_t row_padding = (4 - (row_size % 4)) % 4;
// 	size_t rows_written = 0;
// 	unsigned char *row = (unsigned char *)malloc(row_size + row_padding);
// 	unsigned char *p = &bmpf->data[(bmpf->dhdr.biHeight - 1) * row_size];
// 	fseek(fp, bmpf->bhdr.bOffBits, SEEK_SET);

// 	while (rows_written < bmpf->dhdr.biHeight)
// 	{
// 		if (fread(row, row_size + row_padding, 1, fp) != 1)
// 		{
// 			free(row);
// 			exit(0);
// 		}
// 		if (bytes_per_pixel == 3)
// 		{
// 			for (size_t i = 0; i < row_size; i += bytes_per_pixel)
// 			{
// 				*p = row[i + 2];
// 				p++;
// 				*p = row[i + 1];
// 				p++;
// 				*p = row[i];
// 				p++;
// 			};
// 		}
// 		else if (bytes_per_pixel == 4)
// 		{
// 			for (size_t i = 0; i < row_size; i += bytes_per_pixel)
// 			{
// 				*p = row[i + 3];
// 				p++;
// 				*p = row[i + 2];
// 				p++;
// 				*p = row[i + 1];
// 				p++;
// 				*p = row[i];
// 				p++;
// 			};
// 		}
// 		else
// 		{
// 			printf("err. bytes per pixel = %zu\n", bytes_per_pixel);
// 			exit(0);
// 		};
// 		rows_written++;
// 		p = p - 2 * row_size;
// 	};
// 	free(row);
// }

static size_t c_padding(size_t width) {
    return width % 4 == 0 ? 0 : 4 - (width * 3) % 4;
}

static enum read_status loadBMPFile(FILE *fp, struct image *img)
{

	BMPFile *bmp_file = (BMPFile *)malloc(sizeof(BMPFile));
	if (bmp_file == NULL)
		return BMP_READ_ERROR;

	if (fread(&bmp_file->bhdr, sizeof(struct BMPHeader), 1, fp) != 1)
	{
		free(bmp_file);
		return BMP_READ_ERROR;
	}
	if (fread(&bmp_file->dhdr, sizeof(struct DIBHeader), 1, fp) != 1)
	{
		free(bmp_file);
		return BMP_READ_ERROR;
	}
	size_t data_size = bmp_file->dhdr.biWidth * bmp_file->dhdr.biHeight * bmp_file->dhdr.biBitCount / 8;
	bmp_file->data = (unsigned char *)malloc(data_size);

	if (fseek(fp, bmp_file->bhdr.bOffBits, SEEK_SET) != 0)
		return BMP_READ_ERROR;

	initImage(img, bmp_file->dhdr.biWidth, bmp_file->dhdr.biHeight);
	if (img->data == NULL)
		return BMP_READ_ERROR;

	size_t padding =c_padding(bmp_file->dhdr.biWidth);
	struct pixel *data = img->data;

	for (uint32_t y = 0; y < bmp_file->dhdr.biHeight; y++)
	{
		if (y == bmp_file->dhdr.biHeight - 1)
			padding = 0;
		size_t count = bmp_file->dhdr.biWidth * BYTES_PER_PIXEL + padding;
		if (fread(data, 1, count, fp) != count)
		{
			free(data);
			return BMP_READ_ERROR;
		}
		data += bmp_file->dhdr.biWidth;
	}
	freeBMPFile(bmp_file);
	return BMP_READ_OK;
}

enum read_status from_bmp(char *in, struct image *img)
{
	FILE *fp = fopen(in, "rb");
	if (!fp)
	{
		return BMP_READ_ERROR;
	};
	enum read_status status = loadBMPFile(fp, img);
	fclose(fp);
	return status;
}




static enum write_status saveBMP(FILE *file, struct image const *image)
{
	size_t padding = c_padding(image->width);
	struct pixel *data = image->data;

	size_t image_size = (image->width * 3 + padding) * image->height;

	struct DIBHeader dhdr = {
		.biSize = DIB_HEADER_SIZE_B,
		.biWidth = image->width,
		.biHeight = image->height,
		.biPlanes = 1,
		.biBitCount = BITS_PER_PIXEL,
		.biCompression = BI_RGB,
		.biSizeImage = image_size,
		.biXPelsPerMeter = 0,
		.biYPelsPerMeter = 0,
		.biClrUsed = 0,
		.biClrImportant = 0,
	};

	struct BMPHeader bhdr = {
		.bfType = BMP_TYPE,
		.bfileSize = sizeof(struct DIBHeader) + sizeof(struct BMPHeader) + image_size,
		.bOffBits = sizeof(struct DIBHeader) + sizeof(struct BMPHeader),
	};

	if (fwrite(&bhdr, sizeof(struct BMPHeader), 1, file) != 1)
		return BMP_WRITE_ERROR;
	if (fwrite(&dhdr, sizeof(struct DIBHeader), 1, file) != 1)
		return BMP_WRITE_ERROR;

	for (uint32_t y = 0; y < dhdr.biHeight; y++)
	{
		if (y == dhdr.biHeight - 1)
			padding = 0;

		size_t count = dhdr.biWidth * 3 + padding;
		if (fwrite(data, 1, count, file) != count)
			return BMP_WRITE_ERROR;
		data += dhdr.biWidth;
	}

	return BMP_WRITE_OK;
}

enum write_status to_bmp(char *out, struct image const *img)
{
	FILE *fp = fopen(out, "wb");
	if (fp == NULL)
	{
		return BMP_WRITE_ERROR;
	}
	enum write_status status = saveBMP(fp, img);
	fclose(fp);
	return status;
}

#endif
