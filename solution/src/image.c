#include "image.h"
#include <stdio.h>
#include <stdlib.h>

void initImage(struct image *img, uint64_t width, uint64_t height)
{
    img->width = width;
    img->height = height;
    img->data = calloc(width * height, sizeof(struct pixel));
}

void freeImage(struct image *img)
{
    free(img->data);
    img->data = NULL;
}

struct pixel imageGetPixel(struct image const *img, uint64_t a, uint64_t b)
{
    return img->data[img->width * b + a];
}

void imageSetPixel(struct image *img, uint64_t a, uint64_t b, struct pixel p)
{
    img->data[img->width * b + a] = p;
}
