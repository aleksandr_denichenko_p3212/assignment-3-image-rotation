#include "bmp.h"
#include "image.h"
#include "rotate.h"
#include <stdio.h>
#include <stdlib.h>

int main( int argc, char** argv ) {
	if (argc != 4) {
        fprintf(stderr, "Arguments error");
        return 1;
    }

    char *endptr;
    double value = strtod(argv[3], &endptr);
	if (*endptr != '\0') {
        fprintf(stderr, "Invalid input angle is not a valid.\n");
        return 1;
    }

	struct image source;
	if ((from_bmp(argv[1], &source)) != BMP_READ_OK) {
        fprintf(stderr, "Read error");
        return 1;
    }

	struct image rSource = rotateImage(&source, value);
    if (rSource.data == NULL) {
        fprintf(stderr, "Rotate error");
        return 1;
    }

    if ((to_bmp(argv[2], &rSource)) != BMP_WRITE_OK) {
        fprintf(stderr, "Save error");
        return 1;
    }

	freeImage(&source);
    freeImage(&rSource);

	return 0;
}
