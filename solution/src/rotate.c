#include "rotate.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define M_PI 3.14159265358979323846

uint64_t custom_sin(double x)
{
    double result = 0.0;
    double term = x;
    int i = 1;
    while (term != 0.0)
    {
        result += term;
        term = -term * x * x / ((2 * i) * (2 * i + 1));
        i++;
    }
    return (uint64_t)round(result);
}

uint64_t custom_cos(double x)
{
    double result = 0.0;
    double term = 1.0;
    int i = 0;
    while (term != 0.0)
    {
        result += term;
        term = -term * x * x / ((2 * i + 2) * (2 * i + 1));
        i++;
    }
    return (uint64_t)round(result);
}

struct image rotateImage(struct image const *source, double angle)
{
    struct image result;
    if (fabs(angle) == (double)180)
    {
        initImage(&result, source->width, source->height);
    }
    else if (fabs(angle) == (double)90 || fabs(angle) == (double)270)
    {
        initImage(&result, source->height, source->width);
    }
    else
    {
        initImage(&result, source->width, source->height);
        for (uint64_t i = 0; i < source->width; i++)
        {
            for (uint64_t j = 0; j < source->height; j++)
            {
                imageSetPixel(&result, i, j, imageGetPixel(source, i, j));
            }
        }
        return result;
    }

    if (result.data == NULL)
        return result;

    for (uint64_t i = 0; i < source->width; i++)
    {
        for (uint64_t j = 0; j < source->height; j++)
        {
            uint64_t new_i, new_j;
            if (angle == 90 || angle == -270)
            {
                new_i = j;
                new_j = source->width - 1 - i;
            }
            else if (fabs(angle) == 180)
            {
                new_i = source->width - 1 - i;
                new_j = source->height - 1 - j;
            }
            else
            {
                new_i = source->height - j - 1;
                new_j = i;
            }
            imageSetPixel(&result, new_i, new_j, imageGetPixel(source, i, j));
        }
    }
    return result;
}
